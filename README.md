# Refresher in Computer Science (course given as part of the Master Data Science)

Welcome to the refresher in computer science! Along this course, you will be invited to participate to several labs going through basic knowledge of operating systems, like Linux, to more advanced software engineering skills that can benefit to the quality of the software you contribute to along your career.

## Evaluation

The evaluation of this refresher in computer sciences will be based on 3 grades:
1. Delivery of a personal website hosted on GitHub pages,
2. Delivery of the module's project hosted on GitHub,
3. Evaluation of acquired skills along the labs (multiple choice test).

## Labs

### Lab on Linux (week 01)

As part of this first lab, you will be invited to master the Linux operating system (OS), which is one of the major operating systems used by the scientific and industrial communities.

To do, you will first start with an introductory tutorial to learn about the Linux shell. You are invited to follow this first tutorial online: https://linuxjourney.com/lesson/the-shell

Upon completing this first tutorial, you are invited to explore the tutorial on text manipulation (https://linuxjourney.com/lesson/stdout-standard-out-redirect) as well as regular expressions (https://linuxjourney.com/lesson/regular-expressions-regex).

Finally, if time allows, you can move a step further to create your own shell scripts by following this tutorial:  https://www.learnshell.org

Beyond the shell, the website Linux journey (https://linuxjourney.com) will offer you a lot of resources to understand on Linux works. Feel free to browse the website to increase your technical skills on this topic.

### Lab on Git (week 01)

In the context of the second lab, you will learn about versioning systems, and Git in particular.

After discussing about the benefits of versioning systems and their wide applicability, you will be invited to follow the online tutorial on Git: https://gitimmersion.com/

After completing this tutorial, you can find a nice summary of key git commands on this website: https://rogerdudler.github.io/git-guide/

If you feel a bit puzzled with the way Git works, you can watch this short video: https://www.youtube.com/watch?v=hwP7WQkmECE and/or walk through this illustrated explanation of Git commands: https://marklodato.github.io/visual-git-guide/index-en.html 

Then, for all you projects, you will have to stick to the commit convention that are specified on this website: https://www.conventionalcommits.org/en/v1.0.0/ 

Make sure that you commit messages are conveying meaningful information to help you (and your collaborators) why you actually updated the history of a Git repository to include those specific changes.

### Lab on Python (week 02)

This session is devoted to a refresher on the Python programming language.

For this purpose, you are advised to follow the tutorial available from https://www.learnpython.org and go through the list of chapters. Depending on your programming skills, you can pass faster on the first chapters and focus on the ones you feel less comfortable with.

If you already feel comfortable with all the chapters of the above tutorial, then you can focus on the principles of object-oriented programming, by watching this video: https://www.youtube.com/watch?v=vgeTzWo0pXg

For those of you needing a more in-depth introduction to Python, this online book is a must: https://python101.pythonlibrary.org/

### Lab on TDD (week 02)

This session intends to build upon the previous session to dive into the TDD methodology, which stands for *Test-Driven Development*.

The core idea of TDD is to develop by adopting short programming cycles which all start by writing a test case that fails before moving to the implementation of new features/algorithms.

To learn about this principle, you are invited to watch the following Katas:
- https://www.youtube.com/watch?v=vBJM5pzBfhY
- https://www.youtube.com/watch?v=oF5jTHSV28s
- https://www.youtube.com/watch?v=Ji2KKShNKic

Then, follow this tutorial to learn about the concept of unit test and the principles of TDD: https://rubikscode.net/2021/05/24/test-driven-development-tdd-with-python/


More TDD katas in Python:
- https://www.youtube.com/watch?v=IjIWcky5JK8 
- https://www.youtube.com/watch?v=wUXHXbU9rWQ 

### Lab on clean code (week 04)

This session will focus on best practices for coding (in Python) and delivering so called "*clean code*". Clean code has been introduce by Robert Cecil Martin (aka Uncle Bob) as a [opiniated list of simple rules](https://gist.github.com/wojteklu/73c6914cc446146b8b533c0988cf8d29) to follow in order to keep your code as readable and as easy to understand as possible. The principles underlying clean code can apply to any programming language, including Python : https://blog.devgenius.io/clean-code-in-python-8251eea292fa

By leveraging the TDD principles, you are requested to refactor the code of your ongoing project to clean it. 

More on clean code and coding conventions:
- Lessons from Uncle Bob: https://www.youtube.com/watch?v=7EmboKQH8lM 
- https://github.com/tum-esi/common-coding-conventions

### Lab on design patterns (week 05)

This session will focus on the principles of "*design patterns*". Design patterns are well-suited design solutions to reccurent problems faced when developing software. By knowing about design patterns, software engineers are not expected to reinvent the wheel, but can rather adopted an acknowledged and standardized solution to improve the quality of the software, possibly increasing the modularity of the solution (to better deal with future evolutions) and the quality of the code. You can get a quick demonstration of some of them in this video : https://www.youtube.com/watch?v=bsyjSW46TDg. Yet, there are plenty of design patterns, which are organized into different classes, depending on their purpose: https://github.com/faif/python-patterns 

As part of this lab, you are requested to consider the composite design pattern in your project in order to *i)* design the building as a composition of floors, which are both a `structure` and/or *ii)* design a compound area as an area which is the composition of several sub-areas.

The interested students can also consider the application of other design patterns in the project, like the iterator, the strategy, the factory method, or the visitor. These design patterns can apply both to the business logic of the project and to the associated unit tests you are expected to deliver.

More on design patterns:
- https://sourcemaking.com/design_patterns
- https://python-patterns.guide
- https://python-3-patterns-idioms-test.readthedocs.io/en/latest/PatternConcept.html


## Projects

### Project 1

For this first CS project, you are invited to publish your personal website online via the GitHub platform and the GitHub pages feature it proposes.

Starting from https://guides.github.com/features/pages we ask you to work individually on publishing your personal website. This website should deliver a professional description of your skills (like a CV) and include at least a picture hosted on github, several links (incld. social accounts). You are allowed to fork your website from available templates like https://academicpages.github.io, for example.

Upon completion, your website should be submitted online via this form: https://forms.gle/k2EJdyUyZomDfsSd9

### Project 2

For your second CS project, you are invited to collaborate with a pair to develop the following Python project.

The company of Mr. Guiblind used to deliver office buildings for decades, but modern buildings are getting "smarter", and in particular they are now expected to be delivered together with a *digital twin* in order to support connected scenarios. Typically, a digital twin can be summarized as an application that offers a virtual representation mirroring any physical elements of a building (*e.g.*, wall, door, floor), which are then composed to organize indoor areas (*e.g.*, room, patio, corridor). For example, a given room is designed from the combination of four walls, one containing a door and the others some windows. One of these walls can be shared with a corridor next to the office, etc. Both elements and areas may have specific attributes, but are all including a combination of coordinates to locate them in the building (the coordinates of an area corresponding to the bounding box of all contained assets or sub-areas). The digital twin is expected to live with the building—i.e., if new offices are designed, the digital twin is update accordingly.

This digital twin can then serve different purposes. For example, it can be visited to draw it on a map. For example, tracking a visitor whenever she enters an office at a given time. In that case, the office will keep track of the movements of visitors in order to maintain some statistics (e.g., occupation of the office along a given period of the day).

To better understand the effective usage of a building, we are interested in monitoring which areas are actually visited by people and assets. We assume that the digital twin is connected to an *indoor positioning system* (IPS) that can report on the asset locations reported as timestamped coordinates. Whenever such a position is detected, we want the digital twin to keep track of the associated areas that are visited (areas are not exclusive). To check the inclusion of a position in a potential area, it might be worth to compute the convex hull of an area from the coordinates of enclosed elements (hint: if you do not know how to compute a convex envelope in a 2D plan, [just ask Graham](https://letmegooglethat.com/?q=graham+convex+hul+algorithm)). 

For each of the monitored areas, we are interested in observing the `k` clusters among recorded locations to build some kind of heat map identifying the most popular places in a given area. To implement this feature, you can integrate or implement existing clustering algorithms, like [K-means](https://scikit-learn.org/stable/modules/clustering.html#k-means).

In the context of this project, we are only considering 2D representations of the building (i.e., maps), eventually separating the floors of the building as distinct maps. The elements of the building (e.g., walls) are assumed to have a thickness of `0`, meaning that both the coordinates of both extremities of a wall refers to the center. Hence, a graphical representation of the building would assume a default thickness. 

During the first part of this project, you are therefore invited to propose a Python encoding of the digital twin that Mr. Guiblind can configure whenever he delivers a new building to one of his customers. This digital twin should be delivered as a Git repository and come with a good coverage of the code base, and be developed by following the principles of TDD.

During the second part of this project, you are encouraged to apply the principle of clean code, as introduced above. It might also be relevant to propose a graphical representation of the maps, which can be simply ploted using matplotlib or any library that eases the drawing of maps.

During the third part of this project, you are encouraged to apply and document design patterns that you succeeded to apply to your code.

To bring more clarifications to the above (weak) specification, you are invited to ask questions via the issues of the course: https://gitlab.univ-lille.fr/romain.rouvoy/cs-refresher/-/issues

You should declare your Git repository using this form: https://forms.gle/CiLTM81uHzW5bf7a7

The deadline for delivering the project is **November 5th, 2021**, a snapshot will the be taken by the end of that day.

The project deliverable is expected to contain:
- A **documented project** including all the material to build and run the project, as well as to understand the design decisions,
- A **documented source code** implementing the project in a clear and readable way for a third-party reader,
- A **documented test suite** covering most of the developed code,
- The **self-assessment** form filled by the members of the group.